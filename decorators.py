from functools import wraps
from datetime import datetime

def measure_time(algo):
    @wraps(algo)
    def timer(*args, **kwargs):
        t1 = datetime.utcnow()
        algo(*args, **kwargs)
        print(datetime.utcnow()- t1)
    return timer


        