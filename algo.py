from decorators import measure_time
import random

def generate_input(items):
    l = [random.randint(1,10000000) for j in range(items)]
    return l

@measure_time
def insertion_sort(l):
    for i in range(1, len(l)):
        a = l[i]
        j = i-1
        while l[j] > a and j >= 0:
            l[j+1] = l[j]
            j = j-1
        l[j+1] = a
